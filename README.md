GROUP 35
By William Xu, Jonathan Lai, Joey Periera, Wenqin Chen, Norman Zhong

Act First is an app that alerts medically-trained bystanders of nearby emergencies and assists in first-response procedures. We are focusing on the following three tasks:

1. Notification
Deciding when to receive notifications (sleep mode, night time mode, etc)
What information should be shown in a notification
Acceptance/Declination UX flow

2. Navigation
Navigating not just to the address, but to the patient once at the address

3. Procedure Assistance
Providing useful and easy to understand tools to assist in emergency response
