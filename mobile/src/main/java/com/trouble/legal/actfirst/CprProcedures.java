package com.trouble.legal.actfirst;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

public class CprProcedures extends AppCompatActivity {

    private final String htmlText = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<body><h1>CPR Procedures</h1><p>\n" +
            "\t\t1. Tilt head and lift chin to open airway.<br />\n" +
            "\t\t2. Check for breathing<br />\n" +
            "\t\t3. Give 30 chest compressions, at 100 BPM.<br />\n" +
            "\t\t4. Give 2 rescue breaths<br />\n" +
            "\t\t<br />\n" +
            "\t\tContinue compressions and breaths until:<br />\n" +
            "\t\t\t\t-Patient revives<br />\n" +
            "\t\t\t\t-An AED arrives<br />\n" +
            "</body>\n" +
            "</html>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpr_procedures);

        TextView htmlTextView = (TextView)findViewById(R.id.html_text);
        htmlTextView.setText(Html.fromHtml(htmlText));
        htmlTextView.setTextSize(17);
        htmlTextView.setTextColor(Color.WHITE);

    }

}
