package com.trouble.legal.actfirst.CreateEmergency;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.trouble.legal.actfirst.R;

public class AedOperation extends AppCompatActivity {

        private final String htmlText = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body><h1>Operating an AED</h1><p>\n" +
                "\t\t1. Turn on AED. <br />\n" +
                "\t\t2. Wipe bare chest dry. <br />\n" +
                "\t\t3. Attach pads.<br />\n" +
                "\t\t4. Plug in connector (if necessary)<br />\n" +
                "\t\t5. Stand clear (do not touch person)<br />\n" +
                "\t\t6. Let the AED analyze heart rhythm.<br />\n" +
                "\t\t7. Deliver shock.<br />\n" +
                "\t\t8. After delivering shock, perform CPR.<br />\n" +
                "</body>\n" +
                "</html>\n";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_aed_operation);

            TextView htmlTextView = (TextView)findViewById(R.id.html_text);
            htmlTextView.setText(Html.fromHtml(htmlText));
            htmlTextView.setTextSize(17);
            htmlTextView.setTextColor(Color.WHITE);

        }

    }

