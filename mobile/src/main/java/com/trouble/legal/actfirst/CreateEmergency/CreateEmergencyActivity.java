package com.trouble.legal.actfirst.CreateEmergency;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;

import com.trouble.legal.actfirst.MainActivity;
import com.trouble.legal.actfirst.R;

public class CreateEmergencyActivity extends AppCompatActivity {

    public static final String TAB = "tab";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Double latitude = 37.8756125, longitude = -122.2589924;

        int notificationId = 001;
        // Build intent for notification content
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(TAB, "map");
        intent.putExtra(LATITUDE, latitude);
        intent.putExtra(LONGITUDE, longitude);
        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)

                        .setSmallIcon(R.drawable.ic_warning)
                        .setContentTitle("Cardiac Arrest")
                        .setContentText("0.3 miles away")
                        .setColor(getResources().getColor(R.color.red))
                        .setContentIntent(viewPendingIntent)
                        .addAction(R.drawable.ic_check,
                                "Accept", viewPendingIntent);

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

        // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());

    }
}
