package com.trouble.legal.actfirst;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.trouble.legal.actfirst.CreateEmergency.AedOperation;

public class GuideBookFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    // TODO hard coded values right now
    private String[] procedures = new String[] {"CPR", "Choking", "Bleeding"};
    private String[] equipment = new String[] {"AED", "First Aid kit"};

    private ArrayAdapter<String> proceduresAdapter;
    private ArrayAdapter<String> equipmentAdapter;


    public static GuideBookFragment newInstance() {
        GuideBookFragment fragment = new GuideBookFragment();
        return fragment;
    }
    public GuideBookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_guide_book, container, false);

        proceduresAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, procedures);
        ListView proceduresListView = (ListView) v.findViewById(R.id.guide_procedures_list_view);
        proceduresListView.setAdapter(proceduresAdapter);
        proceduresListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getContext(), "Tapped", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setClass(getActivity(), CprProcedures.class);
                startActivity(intent);
                //TODO: open a new activity and load the proper static asset
            }
        });

        equipmentAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, equipment);
        ListView equipmentListView = (ListView) v.findViewById(R.id.guide_equipment_list_view);
        equipmentListView.setAdapter(equipmentAdapter);
        equipmentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getContext(), "Tapped", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setClass(getActivity(), AedOperation.class);
                startActivity(intent);
                //TODO: open a new activity and load the proper static asset
            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
