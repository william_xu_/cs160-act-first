package com.trouble.legal.actfirst;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAB = "tab";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    private static final String OPEN_PATIENT_TAB = "/patient";

    private SectionsPagerAdapter mSectionsPagerAdapter;

    public static FragmentManager fragmentManager;

    private static ViewPager mViewPager;

    private Double latitude = 0.0, longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(fragmentManager);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    private void processIntent() {
        Intent intent = getIntent();
        String patient = "";
        String tab = "";
        if (intent != null) {
            intent.getStringExtra(OPEN_PATIENT_TAB);
            tab = intent.getStringExtra(TAB);
            latitude = intent.getDoubleExtra(LATITUDE, 0.0);
            longitude = intent.getDoubleExtra(LONGITUDE, 0.0);
        }
        if ("patient".equals(patient)) {
            mViewPager.setCurrentItem(0);
        } else if ("map".equals(tab)) {
            // Norman Here we know they accepted the rescue.
            mViewPager.setCurrentItem(1);

            Intent openWatchIntent = new Intent();
            final String OPEN_WATCH_ACTION = "com.trouble.legal.actfirst.receiver.intent.action.OPEN_WATCH";
            openWatchIntent.setAction(OPEN_WATCH_ACTION);
            sendBroadcast(openWatchIntent);

        }


    }

    @Override
    public void onResume() {
        super.onResume();
        processIntent();

    }

    @Override
    public void onNewIntent(Intent intent) {
        String patient = "";
        String tab = "";
        if (intent != null) {
            intent.getStringExtra(OPEN_PATIENT_TAB);
            tab = intent.getStringExtra(TAB);
            latitude = intent.getDoubleExtra(LATITUDE, 0.0);
            longitude = intent.getDoubleExtra(LONGITUDE, 0.0);
        }
        if ("patient".equals(patient)) {
            mViewPager.setCurrentItem(0);
        } else if ("map".equals(tab)) {
            // Norman Here we know they accepted the rescue.
            mViewPager.setCurrentItem(1);

            Intent openWatchIntent = new Intent();
            final String OPEN_WATCH_ACTION = "com.trouble.legal.actfirst.receiver.intent.action.OPEN_WATCH";
            openWatchIntent.setAction(OPEN_WATCH_ACTION);
            sendBroadcast(openWatchIntent);

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.toggle_silent_mode);
        item.setActionView(R.layout.toggle_switch);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_home) {
//            // Handle the camera action
//        } else if (id == R.id.nav_cert) {
//
//        } else if (id == R.id.nav_tutorial) {
//
//        } else if (id == R.id.nav_settings) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return GuideBookFragment.newInstance();
                case 1:
                    if (latitude == 0.0 || longitude == 0.0) {
                        return ActFirstMapFragment.newInstance();
                    }
                    return ActFirstMapFragment.newInstance(latitude, longitude);
                case 2:
                    String name = "Jonathan Lai";
                    String des = "21 year-old male, Asian";
                    String cond = "Suspected cardiac arrest";
                    String loc = "3nd Floor, inside HP auditorium";
                    return PatientFragment.newInstance(name, des, cond, loc);
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Guide";
                case 1:
                    return "Map";
                case 2:
                    return "Patient";
            }
            return null;
        }
    }

    public static void changePage(int pos) {
        mViewPager.setCurrentItem(pos);
    }
}
