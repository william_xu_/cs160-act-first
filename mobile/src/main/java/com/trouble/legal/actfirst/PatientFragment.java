package com.trouble.legal.actfirst;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.trouble.legal.actfirst.adapters.PatientInfoAdapter;


public class PatientFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private static final String ARG_PARAM1 = "patientName";
    private static final String ARG_PARAM2 = "description";
    private static final String ARG_PARAM3 = "condition";
    private static final String ARG_PARAM4 = "location";

    private String[][] listItems;
    private ArrayAdapter patientInfoAdapter;

    public PatientFragment() {
        // Required empty public constructor
    }


    public static PatientFragment newInstance(String patientName, String description, String condition, String location) {
        PatientFragment fragment = new PatientFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, patientName);
        args.putString(ARG_PARAM2, description);
        args.putString(ARG_PARAM3, condition);
        args.putString(ARG_PARAM4, location);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String patientName = getArguments().getString(ARG_PARAM1);
            String description = getArguments().getString(ARG_PARAM2);
            String condition = getArguments().getString(ARG_PARAM3);
            String location = getArguments().getString(ARG_PARAM4);

            listItems = new String[4][2];
            listItems[0] = new String[] {"Name:", patientName};
            listItems[1] = new String[] {"Description:", description};
            listItems[2] = new String[] {"Condition:", condition};
            listItems[3] = new String[] {"Location:", location};

//            listItems = new String[2][4];
//            listItems[0] = new String[] {"Name:", "Description", "Condition", "Location"};
//            listItems[1] = new String[] {patientName, description, condition, location};
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_patient, container, false);
        patientInfoAdapter = new PatientInfoAdapter(getContext(), R.layout.patient_info_list_item, listItems);
        ListView listView = (ListView) v.findViewById(R.id.patient_list_view);
        listView.setAdapter(patientInfoAdapter);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
