package com.trouble.legal.actfirst;

import android.content.Intent;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

public class WatchListenerService extends WearableListenerService implements MessageApi.MessageListener {

    private static final String ACCEPT_NOTIFICATION = "/accepted";

    @Override
    public void onMessageReceived(MessageEvent event) {

        if (event.getPath().equals(ACCEPT_NOTIFICATION)){
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}