package com.trouble.legal.actfirst.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.trouble.legal.actfirst.R;

public class PatientInfoAdapter extends ArrayAdapter<String[]> {

    Context context;
    int layoutResourceId;
    String[] info;
    String[] content;

    public PatientInfoAdapter(Context context, int layoutResource, String[][] data) {
        super(context, layoutResource, data);
        this.layoutResourceId = layoutResource;
        this.context = context;

        info = new String[]{data[0][0], data[1][0], data[2][0], data[3][0]};
        content = new String[]{data[0][1], data[1][1], data[2][1], data[3][1]};

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PatientInfoHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new PatientInfoHolder();
            holder.info = (TextView) row.findViewById(R.id.col1);
            holder.content = (TextView) row.findViewById(R.id.col2);

            row.setTag(holder);

        } else {
            holder = (PatientInfoHolder) row.getTag();
        }

        holder.info.setText(info[position]);
        holder.content.setText(content[position]);
        return row;
    }

    static class PatientInfoHolder {
        TextView info;
        TextView content;
    }
}
