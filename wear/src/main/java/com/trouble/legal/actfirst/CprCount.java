package com.trouble.legal.actfirst;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

public class CprCount extends WearableActivity {

    String TAG = "CprCount";
    int compression = 0;
    Handler compressionsHandler;
    Runnable cprRunnable;
    TextView countTextView;
    TextView compressionsTextView;
    Vibrator v;
    Boolean onDelay = false;
    Boolean cprMode = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpr_count);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        countTextView = (TextView) findViewById(R.id.cprCounter);
        compressionsTextView = (TextView) findViewById(R.id.compressionStr);
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        cprMode = true;
        incrementCompressions();

    }

    @Override
    public void onStop() {
        compressionsHandler.removeCallbacks(cprRunnable);
        super.onStop();
    }

    public void incrementCompressions() {
        compressionsHandler = new Handler();
        cprRunnable = new Runnable() {
            @Override
            public void run() {
                //if(compression<=5 && !onDelay){
                //compress at 100bpm
                if (compression <= 30) {
                    v.vibrate(100);
                    compressionsTextView.setText(R.string.compressions);
                    countTextView.setTextSize(80);
                    countTextView.setText(String.valueOf(compression));
                    Log.d(TAG, Integer.toString(compression));
                    //vibrate here
                    compression++;
                    compressionsHandler.postDelayed(cprRunnable, 600);
                } else {
                    //first cycle ended. 5 seconds to breathe.
                    compressionsHandler.removeCallbacks(cprRunnable);
                    compressionsHandler.postDelayed(cprRunnable, 5000);
                    Log.d(TAG, "Breathing time");
                    onDelay = true;
                    //have "BREATHE" pop up
                    compressionsTextView.setText("");
                    countTextView.setTextSize(50);
                    countTextView.setText(R.string.breathe);
                    compression = 0;
                }

            }
        };
        cprRunnable.run();
    }
}



