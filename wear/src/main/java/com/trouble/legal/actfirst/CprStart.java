package com.trouble.legal.actfirst;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class CprStart extends Activity {

    String TAG = "CprStart";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpr_start);
        Button start = (Button) findViewById(R.id.begin_cpr_button);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "button pressed");
                Intent intent = new Intent();
                intent.setClass(CprStart.this, CprCount.class);
                startActivity(intent);
            }
        });
    }
}