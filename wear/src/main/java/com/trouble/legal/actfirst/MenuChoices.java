package com.trouble.legal.actfirst;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MenuChoices extends Activity {

    String TAG = "MenuChoices";

    private Button patient_info;
    private Button end_emergency;
    private Button begin_cpr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_choices);
        patient_info = (Button) findViewById(R.id.patient_info);
        end_emergency = (Button) findViewById(R.id.end_emergency);
        begin_cpr = (Button) findViewById(R.id.begin_cpr);

        patient_info.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "patient info clicked");

                final String INTENT_ACTION = "com.trouble.legal.actfirst.receiver.intent.action.PATIENT_TAB";
                Intent intent = new Intent();
                intent.setAction(INTENT_ACTION);
                sendBroadcast(intent);
            }
        });

        end_emergency.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "end emergency clicked");

                end_emergency.setText("Confirm?");
                end_emergency.setTextColor(getResources().getColor(R.color.white));
                RelativeLayout rl = (RelativeLayout) findViewById(R.id.activity_menu_choices_layout);
                rl.setBackgroundResource(R.drawable.exit_new);

                end_emergency.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
        });

        begin_cpr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "begin cpr clicked");
                Intent intent = new Intent();
                intent.setClass(MenuChoices.this, CprStart.class);
                startActivity(intent);
            }
        });
    }

}
