package com.trouble.legal.actfirst;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

public class NoneEmergency extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_emergency_activity);
        final Switch dndSwitch = (Switch) findViewById(R.id.do_not_disturb_switch);
        dndSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.no_emergency_main);
                if(isChecked){
                    layout.setBackgroundResource(R.color.red);
                }
                else {
                    layout.setBackgroundResource(R.color.colorPrimary);
                }
            }
        });
    }
}



